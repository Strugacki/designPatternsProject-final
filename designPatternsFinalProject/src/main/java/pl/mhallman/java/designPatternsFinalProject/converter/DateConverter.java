package pl.mhallman.java.designPatternsFinalProject.converter;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

import java.time.LocalDate;

public class DateConverter implements Converter {

    public static final DateConverter INSTANCE = new DateConverter();

    public String convert(Person person) {
        LocalDate date = LocalDate.now();
        return String.valueOf(date.minusYears(person.getAge()).getYear());
    }

}
