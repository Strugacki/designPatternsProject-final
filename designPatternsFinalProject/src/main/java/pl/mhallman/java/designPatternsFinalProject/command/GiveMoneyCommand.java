package pl.mhallman.java.designPatternsFinalProject.command;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

public class GiveMoneyCommand implements MoneyManagement {

	private Person personToMakeRicher;
	private double value;

	public GiveMoneyCommand(Person personToMakeRicher, final double value) {
		this.personToMakeRicher = personToMakeRicher;
		this.value = value;
	}

	public void execute() {
		personToMakeRicher.giveMoney(value);
	}

}
