package pl.mhallman.java.designPatternsFinalProject.constant;

import lombok.Getter;

@Getter
public enum ClothesType {

    DIRTY("Dirty clothes"),
    CASUAL("Casual clothes"),
    SMART("Smart clothes"),
    UNIFORM("Uniform");

    private String name;

    ClothesType(final String name) {
        this.name = name;
    }

    ;
}
