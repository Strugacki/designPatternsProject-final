package pl.mhallman.java.designPatternsFinalProject.adapter;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;
import pl.mhallman.java.designPatternsFinalProject.domain.PersonDetails;

public interface PersonDetailsAdapter {


    PersonDetails adapt(Person person);




}
