package pl.mhallman.java.designPatternsFinalProject.constant;

public enum KnowledgeLevel {

    WELL_EDUCATED("Well educated person"),
    LESS_EDUCATED("Less educated person"),
    WIHOUT_EDUCATION("Person without education"),
    PRETTY_SMART("Pretty smart person"),
    VERY_INTELIGENT("Very inteligent person");

    private String name;

    KnowledgeLevel(String name) {
        this.name = name;
    }
}
