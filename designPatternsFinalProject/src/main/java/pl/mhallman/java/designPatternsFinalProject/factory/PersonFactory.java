package pl.mhallman.java.designPatternsFinalProject.factory;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

import java.util.HashMap;

public class PersonFactory {

	public static final PersonFactory INSTANCE = new PersonFactory();
	private HashMap registeredPersonsMap = new HashMap();

	public void registerPerson(final String personType, Person person) {
		registeredPersonsMap.put(personType, person);
	}

	public Person getPerson(final String personType) {
		return ((Person) registeredPersonsMap.get(personType)).createPerson();
	}

}
