package pl.mhallman.java.designPatternsFinalProject.command;

import java.util.ArrayList;
import java.util.List;

public class CommandProcessor {

	List<MoneyManagement> commands = new ArrayList<MoneyManagement>();

	public void addCommand(final MoneyManagement command) {
		commands.add(command);
	}

	public void executeCommands() {
		for (MoneyManagement command : commands) {
			command.execute();
		}
	}


}
