package pl.mhallman.java.designPatternsFinalProject.domain;

import pl.mhallman.java.designPatternsFinalProject.constant.ClothesType;
import pl.mhallman.java.designPatternsFinalProject.constant.KnowledgeLevel;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonFactory;
import pl.mhallman.java.designPatternsFinalProject.visitor.Visitor;

public class Politician extends Person {

	static {
		PersonFactory.INSTANCE.registerPerson("politician", new Politician());
	}

	public Politician() {
		this.firstName = "Włodzimierz";
		this.lastName = "Mackizrewicz";
		this.age = 42;
		this.moneyInPocket = 50000.00;
        this.clothesType = ClothesType.SMART;
        this.knowledgeLevel = KnowledgeLevel.WELL_EDUCATED;
    }

	@Override
	public Politician createPerson() {
		return new Politician();
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
