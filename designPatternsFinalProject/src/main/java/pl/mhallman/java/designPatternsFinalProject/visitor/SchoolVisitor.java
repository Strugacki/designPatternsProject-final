package pl.mhallman.java.designPatternsFinalProject.visitor;

import pl.mhallman.java.designPatternsFinalProject.domain.*;


public class SchoolVisitor implements Visitor {

	public void visit(SchoolKid schoolKid) {
		System.out.println("As a kid I am learning a lot of fun stuff in school five days in a week");
	}

	public void visit(Homeless homeless) {
		System.out.println("As a Homeless I regret that I did not learn in school");
	}

	public void visit(Programmer programmer) {
		System.out.println("As a Programmer I can show kids basic of coding during IT classes");
	}

	public void visit(Politician politician) {
		System.out.println("As a Politician I can take patronage over some schools");
	}

	public void visit(CabDriver cabDriver) {
		System.out.println("As a Cab driver I am sometimes picking up teachers from school");
	}

}
