package pl.mhallman.java.designPatternsFinalProject.adapter;

import pl.mhallman.java.designPatternsFinalProject.Facade.ConverterFacade;
import pl.mhallman.java.designPatternsFinalProject.domain.Person;
import pl.mhallman.java.designPatternsFinalProject.domain.PersonDetails;

public class PersonDetailsAdapterImpl implements PersonDetailsAdapter {

    private ConverterFacade converterFacade;

    public PersonDetailsAdapterImpl(ConverterFacade converterFacade) {
        this.converterFacade = converterFacade;
    }

    public PersonDetails adapt(Person person) {
        return converterFacade.convertPerson(new PersonDetails(), person);
    }
}
