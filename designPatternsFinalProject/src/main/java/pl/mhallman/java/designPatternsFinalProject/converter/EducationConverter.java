package pl.mhallman.java.designPatternsFinalProject.converter;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

public class EducationConverter implements Converter {

    public static final EducationConverter INSTANCE = new EducationConverter();

    public String convert(Person person) {
        final String education;
        switch (person.getKnowledgeLevel()) {
            case WELL_EDUCATED:
                education = "Junior high school";
                break;
            case LESS_EDUCATED:
                education = "Primary school";
                break;
            case PRETTY_SMART:
                education = "Upper secondary education";
                break;
            case VERY_INTELIGENT:
                education = "Tertiary education";
                break;
            default:
                education = "Without education";
        }
        return education;
    }
}
