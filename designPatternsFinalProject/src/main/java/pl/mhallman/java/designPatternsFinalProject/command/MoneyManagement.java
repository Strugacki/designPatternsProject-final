package pl.mhallman.java.designPatternsFinalProject.command;

public interface MoneyManagement {
	void execute();
}
