package pl.mhallman.java.designPatternsFinalProject.visitor;

public interface Visitable {
	void accept(Visitor visitor);
}
