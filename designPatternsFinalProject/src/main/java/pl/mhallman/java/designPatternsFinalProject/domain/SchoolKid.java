package pl.mhallman.java.designPatternsFinalProject.domain;

import pl.mhallman.java.designPatternsFinalProject.constant.ClothesType;
import pl.mhallman.java.designPatternsFinalProject.constant.KnowledgeLevel;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonFactory;
import pl.mhallman.java.designPatternsFinalProject.visitor.Visitor;

public class SchoolKid extends Person {

	static {
		PersonFactory.INSTANCE.registerPerson("schoolKid", new SchoolKid());
	}

	public SchoolKid() {
		this.firstName = "Donald";
		this.lastName = "Kieł";
		this.age = 11;
		this.moneyInPocket = 4.99;
        this.clothesType = ClothesType.UNIFORM;
        this.knowledgeLevel = KnowledgeLevel.LESS_EDUCATED;
    }

	@Override
	public SchoolKid createPerson() {
		return new SchoolKid();
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
