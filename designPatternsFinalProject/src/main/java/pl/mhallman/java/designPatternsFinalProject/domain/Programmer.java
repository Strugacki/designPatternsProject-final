package pl.mhallman.java.designPatternsFinalProject.domain;

import pl.mhallman.java.designPatternsFinalProject.constant.ClothesType;
import pl.mhallman.java.designPatternsFinalProject.constant.KnowledgeLevel;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonFactory;
import pl.mhallman.java.designPatternsFinalProject.visitor.Visitor;

public class Programmer extends Person {

	static {
		PersonFactory.INSTANCE.registerPerson("programmer", new Programmer());
	}

	public Programmer() {
		this.firstName = "Bartek";
		this.lastName = "Bramy";
		this.age = 24;
		this.moneyInPocket = 1000.00;
        this.clothesType = ClothesType.CASUAL;
        this.knowledgeLevel = KnowledgeLevel.VERY_INTELIGENT;
    }

	@Override
	public Person createPerson() {
		return new Programmer();
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
