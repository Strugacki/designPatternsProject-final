package pl.mhallman.java.designPatternsFinalProject.factory;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

public class PersonProvider {

	private PersonFactory factory;

	public PersonProvider(PersonFactory factory) {
		this.factory = factory;
	}

	public Person getPerson(final String personType) {
		return factory.getPerson(personType);
	}
}
