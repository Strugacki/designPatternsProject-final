package pl.mhallman.java.designPatternsFinalProject;

import pl.mhallman.java.designPatternsFinalProject.Facade.ConverterFacade;
import pl.mhallman.java.designPatternsFinalProject.adapter.PersonDetailsAdapterImpl;
import pl.mhallman.java.designPatternsFinalProject.command.CommandProcessor;
import pl.mhallman.java.designPatternsFinalProject.command.GiveMoneyCommand;
import pl.mhallman.java.designPatternsFinalProject.command.TakeMoneyCommand;
import pl.mhallman.java.designPatternsFinalProject.converter.DateConverter;
import pl.mhallman.java.designPatternsFinalProject.converter.EducationConverter;
import pl.mhallman.java.designPatternsFinalProject.converter.JobConverter;
import pl.mhallman.java.designPatternsFinalProject.domain.*;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonFactory;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonProvider;
import pl.mhallman.java.designPatternsFinalProject.visitor.CarVisitor;
import pl.mhallman.java.designPatternsFinalProject.visitor.GovernmentVisitor;
import pl.mhallman.java.designPatternsFinalProject.visitor.SoftwareHouseVisitor;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Class.forName;
import static java.util.Arrays.asList;

/**
 * Hello world!
 */
public class App {

	static {
		try {
            forName("pl.mhallman.java.designPatternsFinalProject.domain.CabDriver");
            forName("pl.mhallman.java.designPatternsFinalProject.domain.Homeless");
            forName("pl.mhallman.java.designPatternsFinalProject.domain.Politician");
            forName("pl.mhallman.java.designPatternsFinalProject.domain.Programmer");
            forName("pl.mhallman.java.designPatternsFinalProject.domain.SchoolKid");
        } catch (ClassNotFoundException any) {
			any.printStackTrace();
		}
	}

	public static void main(String[] args) {
		List<Person> personList = new ArrayList<Person>();

		//CREATIONAL :: Factory + Singleton
		PersonProvider personProvider = new PersonProvider(PersonFactory.INSTANCE);
		CabDriver cabDriver = (CabDriver) personProvider.getPerson("cabDriver");
		SchoolKid schoolKid = (SchoolKid) personProvider.getPerson("schoolKid");
		Homeless homeless = (Homeless) personProvider.getPerson("homeless");
		Politician politician = (Politician) personProvider.getPerson("politician");
		Programmer programmer = (Programmer) personProvider.getPerson("programmer");

		personList.addAll(asList(cabDriver, schoolKid, homeless, politician, programmer));

		//BEHAVIORAL :: Command
		CommandProcessor commandProcessor = new CommandProcessor();
		commandProcessor.addCommand(new GiveMoneyCommand(homeless, 100.0));
		commandProcessor.addCommand(new TakeMoneyCommand(homeless, 50.0));
		commandProcessor.addCommand(new GiveMoneyCommand(schoolKid, 10.0));
		commandProcessor.addCommand(new TakeMoneyCommand(schoolKid, 12.0));
		System.out.println("--TESTING COMMAND PATTERN--");
		commandProcessor.executeCommands();

		//BEHAVIORAL :: Visitor
		GovernmentVisitor governmentVisitor = new GovernmentVisitor();
		CarVisitor carVisitor = new CarVisitor();
		SoftwareHouseVisitor softwareHouseVisitor = new SoftwareHouseVisitor();
		System.out.println("--TESTING VISITOR PATTERN--");
		politician.accept(carVisitor);
		homeless.accept(governmentVisitor);
		programmer.accept(softwareHouseVisitor);
		schoolKid.accept(governmentVisitor);

        //BEHAVIORAL :: ADAPTER + FACADE
        ConverterFacade converterFacade = new ConverterFacade(DateConverter.INSTANCE, JobConverter.INSTANCE, EducationConverter.INSTANCE);
        PersonDetailsAdapterImpl adapter = new PersonDetailsAdapterImpl(converterFacade);

        PersonDetails programmerDetails = adapter.adapt(programmer);
        PersonDetails homelessDetails = adapter.adapt(homeless);
        PersonDetails schoolKidDetails = adapter.adapt(schoolKid);

        System.out.println("--TESTING ADAPTER+FACADE PATTERNS--");
        System.out.println("Programmer Details: " + programmerDetails);
        System.out.println("Homeless Details: " + homelessDetails);
        System.out.println("SchoolKid Details: " + schoolKidDetails);


	}
}
