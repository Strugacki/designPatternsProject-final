package pl.mhallman.java.designPatternsFinalProject.command;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

public class TakeMoneyCommand implements MoneyManagement {

	private Person personToMakePoorer;
	private double value;

	public TakeMoneyCommand(final Person personToMakePoorer, final double value) {
		this.personToMakePoorer = personToMakePoorer;
		this.value = value;
	}

	public void execute() {
		personToMakePoorer.takeMoney(value);
	}
}
