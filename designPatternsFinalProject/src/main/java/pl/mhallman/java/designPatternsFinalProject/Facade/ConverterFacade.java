package pl.mhallman.java.designPatternsFinalProject.Facade;

import pl.mhallman.java.designPatternsFinalProject.converter.DateConverter;
import pl.mhallman.java.designPatternsFinalProject.converter.EducationConverter;
import pl.mhallman.java.designPatternsFinalProject.converter.JobConverter;
import pl.mhallman.java.designPatternsFinalProject.domain.Person;
import pl.mhallman.java.designPatternsFinalProject.domain.PersonDetails;

public class ConverterFacade {

    private DateConverter dateConverter;
    private JobConverter jobConverter;
    private EducationConverter educationConverter;

    public ConverterFacade(DateConverter dateConverter, JobConverter jobConverter, EducationConverter educationConverter) {
        this.dateConverter = dateConverter;
        this.jobConverter = jobConverter;
        this.educationConverter = educationConverter;
    }

    public PersonDetails convertPerson(PersonDetails details, Person person) {
        details.setFullname(person.getFirstName() + " " + person.getLastName());
        details.setEducation(educationConverter.convert(person));
        details.setJob(jobConverter.convert(person));
        details.setYearOfBirth(dateConverter.convert(person));
        return details;
    }
}
