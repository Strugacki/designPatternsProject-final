package pl.mhallman.java.designPatternsFinalProject.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.mhallman.java.designPatternsFinalProject.constant.ClothesType;
import pl.mhallman.java.designPatternsFinalProject.constant.KnowledgeLevel;
import pl.mhallman.java.designPatternsFinalProject.visitor.Visitable;

@Getter
@Setter
@NoArgsConstructor
public abstract class Person implements Visitable {

    protected String firstName;

    protected String lastName;

    protected ClothesType clothesType;

    protected KnowledgeLevel knowledgeLevel;

    protected int age;

    protected double moneyInPocket;

    public abstract Person createPerson();

    public void giveMoney(final double money) {
        System.out.println("I gave " + money + " money to " + this.getClass().getSimpleName());
        this.moneyInPocket += money;
    }

    public void takeMoney(final double money) {
        System.out.println("I took " + money + " money from " + this.getClass().getSimpleName());
        this.moneyInPocket -= money;
    }

}
