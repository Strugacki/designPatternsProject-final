package pl.mhallman.java.designPatternsFinalProject.visitor;

import pl.mhallman.java.designPatternsFinalProject.domain.*;


public class StreetVisitor implements Visitor {

	public void visit(SchoolKid schoolKid) {
		System.out.println("As a kid I sometimes play on streets with my friends");
	}

	public void visit(Homeless homeless) {
		System.out.println("As a Homeless I live on the street");
	}

	public void visit(Programmer programmer) {
		System.out.println("As a Programmer I sometimes look on the street from the window of my office");
	}

	public void visit(Politician politician) {
		System.out.println("As a Politician I am not really interested what is going on on the street");
	}

	public void visit(CabDriver cabDriver) {
		System.out.println("As a Cab driver I sometimes throw chewing gum on the street");
	}
}
