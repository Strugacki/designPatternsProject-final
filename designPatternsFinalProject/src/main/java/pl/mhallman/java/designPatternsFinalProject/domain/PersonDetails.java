package pl.mhallman.java.designPatternsFinalProject.domain;

import lombok.Data;

@Data
public class PersonDetails {

    String fullname;

    String yearOfBirth;

    String job;

    String education;

}
