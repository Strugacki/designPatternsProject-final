package pl.mhallman.java.designPatternsFinalProject.converter;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

public class JobConverter implements Converter {

    public static final JobConverter INSTANCE = new JobConverter();

    public String convert(Person person) {
        final String job;
        final String className = person.getClass().getSimpleName();
        switch (className) {
            case "CabDriver":
                job = "Taxi driver";
                break;
            case "Programmer":
                job = "Software developer";
                break;
            case "Politician":
                job = "Government";
                break;
            default:
                job = "Without paid job";
        }
        return job;
    }

}
