package pl.mhallman.java.designPatternsFinalProject.domain;


import lombok.Getter;
import lombok.Setter;
import pl.mhallman.java.designPatternsFinalProject.constant.ClothesType;
import pl.mhallman.java.designPatternsFinalProject.constant.KnowledgeLevel;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonFactory;
import pl.mhallman.java.designPatternsFinalProject.visitor.Visitor;

@Getter
@Setter
public class CabDriver extends Person {

	static {
		PersonFactory.INSTANCE.registerPerson("cabDriver", new CabDriver());
	}

	public CabDriver() {
		this.firstName = "Heniek";
		this.lastName = "Wąsacz";
		this.age = 44;
		this.moneyInPocket = 100.00;
        this.clothesType = ClothesType.CASUAL;
        this.knowledgeLevel = KnowledgeLevel.PRETTY_SMART;
    }

	@Override
	public CabDriver createPerson() {
		return new CabDriver();
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
