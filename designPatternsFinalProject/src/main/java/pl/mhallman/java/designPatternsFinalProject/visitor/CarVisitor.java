package pl.mhallman.java.designPatternsFinalProject.visitor;

import pl.mhallman.java.designPatternsFinalProject.domain.*;


public class CarVisitor implements Visitor {

	public void visit(SchoolKid schoolKid) {
		System.out.println("As a kid I can only dream about driving a car");
	}

	public void visit(Homeless homeless) {
		System.out.println("As a Homeless I can steal car and sell it to rent a flat");
	}

	public void visit(Programmer programmer) {
		System.out.println("As a Programmer I can buy a nice car");
	}

	public void visit(Politician politician) {
		System.out.println("As a Politician I have my personal chauffeur");
	}

	public void visit(CabDriver cabDriver) {
		System.out.println("As a Cab driver I am driving people around the city");
	}
}
