package pl.mhallman.java.designPatternsFinalProject.domain;

import pl.mhallman.java.designPatternsFinalProject.constant.ClothesType;
import pl.mhallman.java.designPatternsFinalProject.constant.KnowledgeLevel;
import pl.mhallman.java.designPatternsFinalProject.factory.PersonFactory;
import pl.mhallman.java.designPatternsFinalProject.visitor.Visitor;

public class Homeless extends Person {

	static {
		PersonFactory.INSTANCE.registerPerson("homeless", new Homeless());
	}

	public Homeless() {
		this.firstName = "Stanisław";
		this.lastName = "Stachurski";
		this.age = 65;
		this.moneyInPocket = 7.00;
        this.clothesType = ClothesType.DIRTY;
        this.knowledgeLevel = KnowledgeLevel.WIHOUT_EDUCATION;
    }

	@Override
	public Homeless createPerson() {
		return new Homeless();
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
