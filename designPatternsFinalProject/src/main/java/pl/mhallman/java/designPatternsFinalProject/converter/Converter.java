package pl.mhallman.java.designPatternsFinalProject.converter;

import pl.mhallman.java.designPatternsFinalProject.domain.Person;

public interface Converter {

    String convert(Person person);

}
