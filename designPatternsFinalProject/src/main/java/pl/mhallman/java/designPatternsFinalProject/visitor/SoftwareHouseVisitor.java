package pl.mhallman.java.designPatternsFinalProject.visitor;

import pl.mhallman.java.designPatternsFinalProject.domain.*;


public class SoftwareHouseVisitor implements Visitor {

	public void visit(SchoolKid schoolKid) {
		System.out.println("As a kid I wish I will be smart enough to be a programmer");
	}

	public void visit(Homeless homeless) {
		System.out.println("As a Homeless I sometimes get money from people working in Software Houses");
	}

	public void visit(Programmer programmer) {
		System.out.println("As a Programmer I work in a Software House");
	}

	public void visit(Politician politician) {
		System.out.println("As a Politician I invest in the Software Houses because IT is the future");
	}

	public void visit(CabDriver cabDriver) {
		System.out.println("As a Cab driver I am sometimes picking up programmers from Software Houses");
	}
}
