package pl.mhallman.java.designPatternsFinalProject.visitor;

import pl.mhallman.java.designPatternsFinalProject.domain.*;


public class GovernmentVisitor implements Visitor {

	public void visit(SchoolKid schoolKid) {
		System.out.println("As a kid I learn about government in school");
	}

	public void visit(Homeless homeless) {
		System.out.println("As a Homeless I hate government");
	}

	public void visit(Programmer programmer) {
		System.out.println("As a Programmer I can hack president computer");
	}

	public void visit(Politician politician) {
		System.out.println("As a Politician I try to make a lot of money and look like I want to help people");
	}

	public void visit(CabDriver cabDriver) {
		System.out.println("As a Cab driver I am sometimes picking up people from government");
	}
}
