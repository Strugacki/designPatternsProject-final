package pl.mhallman.java.designPatternsFinalProject.visitor;

import pl.mhallman.java.designPatternsFinalProject.domain.*;


public interface Visitor {

	void visit(final SchoolKid schoolKid);
	void visit(final Homeless homeless);
	void visit(final Programmer programmer);
	void visit(final Politician politician);
	void visit(final CabDriver cabDriver);


}
